<?php

namespace app\core\db;

/**
 * Интерфейс "IDatabase".
 *
 * @package app\core\db
 */
interface IDatabase
{
    /**
     * @return string
     */
    public static function tableName(): string;
}
