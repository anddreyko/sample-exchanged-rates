<?php


namespace app\core\cache;

/**
 * Интерфейс "ICache".
 *
 * @package app\core\cache
 */
interface ICache
{
    /**
     * @param mixed $mValue
     *
     * @return void
     */
    public function setVars($mValue);

    /**
     * @return mixed
     */
    public function getVars();
}
