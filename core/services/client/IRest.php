<?php

namespace app\core\services\client;

/**
 * Интерфейс "IRest".
 *
 * @package app\core\services\client
 */
interface IRest
{
    /**
     * Получение ответа по запросу.
     *
     * @return string
     */
    public function getResponse(): string;

    /**
     * Получение настроек соединения с REST-сервисом.
     * Должен содержать массив настроек для соединения CURL.
     *
     * @link http://php.net/manual/ru/function.curl-setopt.php Подробнеее о параметрах.
     *
     * @return array
     */
    public function getOptions(): array;
}
