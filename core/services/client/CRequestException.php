<?php

namespace app\core\services\client;

use Exception;

/**
 * Класс "CRequestException".
 * Класс исключений запросов на внешние источники.
 *
 * @package app\core\services\client
 */
class CRequestException extends Exception
{
}
