<?php

namespace app\core\services\client;

/**
 * Интерфейс "ISoap".
 *
 * @package app\core\services\client
 */
interface ISoap
{
    /**
     * Вызов метода __soapCall.
     *
     * @return object
     */
    public function getResponse(): object;

    /**
     * Получение настроек соединения с SOAP-сервисом.
     * Должен содержать:
     * - ключ "wsdl" со строкой URL-адреса сервиса.
     * - ключ "options" с массивом настроек.
     *
     * @link http://php.net/manual/ru/soapclient.soapclient.php Подробнее о параметрах.
     *
     * @return array
     */
    public function getOptionsConnection();
}
