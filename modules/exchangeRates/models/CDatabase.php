<?php

namespace app\modules\exchangeRates\models;

use app\core\db\IDatabase;

/**
 * Класс "CDatabase".
 *
 * @package app\modules\exchangeRates\models
 */
class CDatabase implements IApi, IDatabase
{
    /**
     * @return string
     */
    public static function tableName(): string
    {
        // TODO: Implement tableName() method.
        return "";
    }

    /**
     * @param string $sCurrentCurrencyType
     * @param string $sTargetCurrencyType
     * @param float $fValue
     *
     * @return array
     */
    public function exchange(string $sCurrentCurrencyType, string $sTargetCurrencyType, float $fValue): array
    {
        // TODO: Implement exchange() method.
        return [];
    }

    /**
     * @param string $sCurrencyType
     *
     * @return float
     */
    public function getCourse(string $sCurrencyType): float
    {
        // TODO: Implement getCourse() method.
        return (float)0;
    }
}
