<?php

namespace app\modules\exchangeRates\models;

use app\core\cache\ICache;

/**
 * Класс "CCache".
 *
 * @package app\modules\exchangeRates\models
 */
class CCache implements IApi, ICache
{
    /**
     * @param string $sCurrentCurrencyType
     * @param string $sTargetCurrencyType
     * @param float $fValue
     *
     * @return array
     */
    public function exchange(string $sCurrentCurrencyType, string $sTargetCurrencyType, float $fValue): array
    {
        // TODO: Implement exchange() method.
        return [];
    }

    /**
     * @param string $sCurrentCurrencyType
     *
     * @return float
     */
    public function getCourse(string $sCurrentCurrencyType): float
    {
        // TODO: Implement getCourse() method.
        return (float)0;
    }

    /**
     * @param $mValue
     */
    public function setVars($mValue)
    {
        // TODO: Implement setVars() method.
    }

    /**
     * @return mixed
     */
    public function getVars()
    {
        // TODO: Implement getVars() method.
        return null;
    }
}
