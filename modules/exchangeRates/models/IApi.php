<?php


namespace app\modules\exchangeRates\models;

/**
 * Интерфейс "IApi".
 *
 * @package app\modules\exchangeRates\models
 */
interface IApi
{
    /**
     * Получение рассчитанного значения валют.
     *
     * @param string $sCurrentCurrencyType
     * @param string $sTargetCurrencyType
     * @param float $fValue
     *
     * @return array
     */
    public function exchange(string $sCurrentCurrencyType, string $sTargetCurrencyType, float $fValue): array;

    /**
     * Получение курса указанной валюты.
     *
     * @param string $sCurrencyType Тип валюты.
     *
     * @TODO: Тип валюты можно модернизировать до объекта.
     *
     * @return float
     */
    public function getCourse(string $sCurrencyType): float;
}
