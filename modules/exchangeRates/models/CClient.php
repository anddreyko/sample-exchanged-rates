<?php


namespace app\modules\exchangeRates\models;

use Exception;

/**
 * Класс "CClient".
 *
 * @package app\modules\exchangeRates\models
 */
class CClient implements IApi
{
    /** @var \app\modules\exchangeRates\models\CCache */
    private $obCache;

    /** @var \app\modules\exchangeRates\models\CDatabase */
    private $obDatabase;

    /** @var \app\modules\exchangeRates\models\CRest */
    private $obRest;


    /**
     * Конструктор "CClient".
     *
     * @param \app\modules\exchangeRates\models\CCache $obCache
     * @param \app\modules\exchangeRates\models\CDatabase $obDatabase
     * @param \app\modules\exchangeRates\models\CRest $obRest
     */
    public function __construct(CCache $obCache, CDatabase $obDatabase, CRest $obRest)
    {
        $this->obCache = $obCache;
        $this->obDatabase = $obDatabase;
        $this->obRest = $obRest;
    }

    /**
     * @param string $sCurrentCurrencyType
     * @param string $sTargetCurrencyType
     * @param float $fValue
     *
     * @return array
     * @throws \Exception
     */
    public function exchange(string $sCurrentCurrencyType, string $sTargetCurrencyType, float $fValue): array
    {
        $mData = $this->obCache->exchange($sCurrentCurrencyType, $sTargetCurrencyType, $fValue);
        if ($mData) return $mData;

        $mData = $this->obDatabase->exchange($sCurrentCurrencyType, $sTargetCurrencyType, $fValue);
        if ($mData) {
            $this->obCache->setVars($mData);
            return $mData;
        }

        $mData = $this->obRest->exchange($sCurrentCurrencyType, $sTargetCurrencyType, $fValue);
        if ($mData) return $mData;

        throw new Exception;
    }

    /**
     * @param string $sCurrencyType
     *
     * @return float
     */
    public function getCourse($sCurrencyType): float
    {
        // TODO: Implement getCourse() method.
        return (float)0;
    }
}
