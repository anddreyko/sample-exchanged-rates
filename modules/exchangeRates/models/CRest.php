<?php


namespace app\modules\exchangeRates\models;

use app\core\services\client\IRest;

/**
 * Класс "CRest".
 *
 * @package app\modules\exchangeRates\models
 */
class CRest implements IApi, IRest
{
    /**
     * @param string $sCurrentCurrencyType
     * @param string $sTargetCurrencyType
     * @param float $fValue
     *
     * @return array
     */
    public function exchange(string $sCurrentCurrencyType, string $sTargetCurrencyType, float $fValue): array
    {
        // TODO: Implement exchange() method.
        return [];
    }

    /**
     * @param string $sCurrencyType
     *
     * @return float
     */
    public function getCourse(string $sCurrencyType): float
    {
        // TODO: Implement getCourse() method.
        return (float)0;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        // TODO: Implement getOptions() method.
        return [];
    }

    /**
     * Получение ответа по запросу.
     *
     * @return string
     */
    public function getResponse(): string
    {
        // TODO: Implement getResponse() method.
        return "";
    }
}
